#!/bin/bash
#
# GNOME Activity Journal
#
# Copyright © 2021 The GNOME Activity Journal developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

finish() {
    cd -
}

trap finish EXIT

cd "$DIR"

./setup.py build &&
    glib-compile-schemas build/share/glib-2.0/schemas/ &&
    GSETTINGS_SCHEMA_DIR=build/share/glib-2.0/schemas/ ./gnome-activity-journal "$@"
