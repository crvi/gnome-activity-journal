## Dependencies

Required:

- GTK 3.24 or later ( gir bindings )
- python3-zeitgeist 1.0.3 or later
- gnomedesktop-3.0 ( gir bindings )
- ayatanaappindicator3 or appindicator3 ( gir bindings )
- pango-1.0 ( gir bindings )
- python3-gi-cairo
- python3-dbus
- python3-gi
- python3-cairo
- python3-xdg
- python3-distutils
- python3-distutils-extra
- dbus-x11

Optional:

- gstreamer1.0 gir bindings - 1.16 or later ( for audio preview feature )
- gstreamer1.0-gtk3 - 1.16 or later ( for video preview feature )
- python3-pip ( for manual system-wide install )

