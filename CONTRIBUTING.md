To contribute, open merge requests at [New merge
request](https://gitlab.gnome.org/crvi/gnome-activity-journal/-/merge_requests)

Commit messages should follow the [GNOME commit message
guidelines](https://wiki.gnome.org/Git/CommitMessages). We require an
URL to either an issue or a merge request in each commit.
