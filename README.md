# GNOME Activity Journal

GNOME Activity Journal is a tool for easily browsing and finding files
that you worked on. It uses [zeitgeist][zeitgeist] backend store, to
get information and metadata on what files/websites/contacts/etc. you
worked with. It also keeps a chronological journal of all file
activity and supports tagging and establishing relationships between
groups of files.

For more information about GNOME Activity Journal, see the [project
wiki][project-wiki].

## Dependencies

For list of package dependencies, see [here][DEPENDENCIES.md].

## Installation

### For Debian / Ubuntu

```console
$ sudo apt-get install gnome-activity-journal
```

### For Fedora

```console
$ sudo dnf install gnome-activity-journal
```

See [repology][repology] to check if this package is part of your
distribution. If not, you can try either [Manual
Installation][INSTALL.manual] or [Run Without
Installation][INSTALL.noinstall]

## Bugs

Bugs should be reported to the GNOME [bug tracking system][bug-tracker].

## Contributing

To contribute, open merge requests [New Merge Request][new-merge-request]

Commit messages should follow the [GNOME commit message
guidelines](https://wiki.gnome.org/Git/CommitMessages). We require an URL
to either an issue or a merge request in each commit.

## License

GNOME Activity Journal is distributed under the terms of the GNU General Public License,
version 3 or later. See the [COPYING][license] file for details.

Artwork is distributed under Creative Commons Attribution Share-Alike
License. See [CC BY-SA 3.0][cc-by-sa-3] for details.

## Copyright

See [AUTHORS][authors] and the source file's headers for the copyright holder's names.

Additionally, some images use textures or other resources from 3rd parties
not directly involved with the project. Those are:

 org.gnome.ActivityJournal.svg:
  - Luigi Chiesa, 2007
    http://commons.wikimedia.org/wiki/File:Jute_nahtlos.png

## Code Of Conduct
We follow the [GNOME Code of Conduct][gnome-coc].
All communications in project spaces are expected to follow it.

[zeitgeist]: https://gitlab.freedesktop.org/zeitgeist/zeitgeist
[project-wiki]: https://wiki.gnome.org/Apps/ActivityJournal
[bug-tracker]: https://gitlab.gnome.org/crvi/gnome-activity-journal/issues
[gnome-coc]: https://wiki.gnome.org/Foundation/CodeOfConduct
[new-merge-request]: https://gitlab.gnome.org/crvi/gnome-activity-journal/-/merge_requests/new
[cc-by-sa-3]: http://creativecommons.org/licenses/by-sa/3.0/
[repology]: https://repology.org/project/gnome-activity-journal/versions
[license]: COPYING
[authors]: AUTHORS
[INSTALL.manual]: INSTALL.md#manual-installation
[INSTALL.noinstall]: INSTALL.md#run-without-installation
[DEPENDENCIES.md]: DEPENDENCIES.md#dependencies
