2021-02-01: GNOME Activity Journal 1.0.0 "Blue"
-----------------------------------------------

* Add following options to preferences

  - General > Day labels customization
  - General > Use separate zoom levels for each view
  - General > Hide day labels when there are no events
  - General > Confirm item deletion
  - General > Minimize app to system tray on close
  - Views > Timeline View > Show day / night gradient in Timeline header
  - Views > Timeline View > Show thumbnails rather than mime-type icons
  - Views > Thumbail View > center or fill thumbnail
  - Views > Multi View > Number of days to show in multiview
  - Search & Preview > Search Results > Large text style
  - Search & Preview > Search Results > Bold text style
  - Search & Preview > Search Criteria > Ignore single character searches
  - Search & Preview > Search > Case sensitivity and path search
  - Search & Preview > Search results > Customize highlight colors
  - Search & Preview > Preview > Show audio preview for audio items
  - Search & Preview > Preview > Show video preview for video items

* General features and fixes

 - Move from GTK2 to GTK3
 - Move from python2 to python3
 - Move to GtkApplication
 - Switch to use GtkHeaderBar
 - Use GtkSearchBar for search
 - Save current zoom level for view
 - Use descriptive names rather than GAJ
 - MultiView: Display no events label for days with no events
 - TimelineView: Display no events label for days with no events
 - ThumbView: Display no events label for days with no events
 - TimelineHeader: Add day / night gradient
 - Switch to Gtk.Grid with vertical orientation rather than Gtk.VBox
 - Add support to jump to selected day from calendar
 - Make video preview work with Gstreamer1.0
 - Show normal static thumbnail preview if video preview is not available
 - Show normal static thumbnail preview if audio preview is not available
 - Get rid of Viewport changes as this is done automatically by GtkContainer
 - Move preference settings to preferences subfolder in schema
 - EraseMode: Make deletion from all events to single event
 - Rework and refactor item delete code
 - Minimal support for dark mode themes
 - Search: Highlight matching days in day label
 - Search: Display search progress in search entry
 - Search: Highlight matching count in category box
 - Search: Make UI responsive during search
 - Search: Update GTK widgets in idle loop rather than threads
 - Display more descriptive messages is error cases
 - About dialog cleanup

2011-7-16: GNOME Activity Journal 0.8.0 "Sword"
-----------------------------------------------

 - Added an incognito mode option to the tray icon.
 - Removed blacklist plugin, replaced by Activity Log Manager.
 - Added different zoom levels to ThumbView and TimelineView.
 - Added an erase mode to easily delete many events.
 - Improved handling of the extension not being available (LP: #790834).
 - Added support for file annotation meta-data (LP: #715910).
 - Don't show application launch events nor fake events generated
   by Activity Log Manager.
 - Fixed blacklist template deletion (LP: #643795).
 - Several other minor tweaks (better netbook screen support, etc).

2011-1-26: GNOME Activity Journal 0.6.0 "Pink Unicorns don't exist"
-------------------------------------------------------------------
* General New features
 - Added drag and drop support to the three views (LP: #553385).
 - Added drag and drop support to the bookmark/pin area (LP: #365048).
 - Improved items preview adding information tooltip. (LP: #561863).
 - Added tooltip for peeking quickly into categories.
 - Added preview for Audio items.
 - Added appindicator/Tray icon support.
 - Added support for Xchat.
 - Added support for Bazaar version control system.
 
* Eye candy tweaks
 - Added "welcome screen" showed when the Activity Journal starts and loads the items.
 - Go to Today button always highlighted.
 - Path's label clickable in MoreInformation window (LP: #532303).
 - Usability tweaks to the views toolbar.
 - Item's categories becomes coloured when containing a searched item.
 
* Bug fixes
 - Fixed bookmark/pinning feature (LP: #680653).
 - Fixed support for RECEIVE_EVENT (LP: #667870).
 - Fixed day's part timerange (LP: #655045).
 - Fixed crash caused by gstreamer (LP: #705545).
 - Fixed problem with dialog windows (LP: #706543).
 - Fixed title of MoreInformation window(LP: #706544).
 - Fixed video previews problem.
 - Fixed visualization bug in TimeLineView.
 - Fixed internationalization and localization problems.
 
* Misc
 - Added support for DELETE_EVENT (fx. deleted Tomboy's notes aren't showed).
 - Cleaned up code (fixed some gtk and gio warnings, remove unused code).
 - Added new translations (ast, it, el, eo, et, eu, ko, sl, zh_TW). 


2010-09-18: GNOME Activity Journal 0.5.0.1 "Bazaar"
---------------------------------------------------

This is a bug-fix release for GNOME Activity Journal 0.5.0.

 - Updated Hamster-specific code to use the new ontology from Zeitgeist 0.5.0
   (LP: #641148, Debian bug #591267).
 - Catch exception trying to extract a non-existant thumbnail from an
   OpenDocument file (LP: #535440).
 - Fixed a possible issue with "%20" being used in filenames.
 - Some minor changes (relabeled "Goto Today" to "Go to Today", added missing
   tooltips to toolbar icons).
 - Removed superfluous debugging (timing) information.
 - Updated several translations.
 - Removed Zeitgeist version check for Zeitgeist 0.5.0 compatibility (as it
   reports a wrong version number).

2010-08-24: GNOME Activity Journal 0.5.0 "Defying Gravity"
----------------------------------------------------------

 - Reduced startup time and improved responsivity.
 - Improved support for Tomboy notes and for websites.
 - Added an (experimental) toolbar providing easier access to several options.
 - Improved the search tool.
 - Added a preferences dialog providing access to a Blacklist management
   extension.

2010-05-02: GNOME Activity Journal 0.3.4.1
------------------------------------------

This release avoids an error when using Python 2.5 due to its lack of new-style
string formatting.

2010-05-01: GNOME Activity Journal 0.3.4
----------------------------------------

 - Changed the timeline view to be based upon GtkTreeView and also revamped
   the multiple day view.
 - Improved the rendering of the thumbnail view.
 - Improved support for dark themes, and fixed widget drawing when a theme
   with pixbuf backgrounds is in use.
 - Added a "More information" dialogue for events, showing information about
   them and pointing to related subjects.
 - The contextual menu is now available in all widgets.
 - Added a content object system which allows users to launch, view, and
   manipulate events with non-file subjects.

...
